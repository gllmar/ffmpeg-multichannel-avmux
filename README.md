# generate_test_media.py 

# Sample Video Generator with 8-channel Audio Tone

This script utilizes `ffmpeg` to generate a video with an 8-channel audio tone. Each channel of the audio corresponds to a different frequency, derived from MIDI notes. The generated video shows a test pattern and has a timecode overlay on it.

## Features:

1. Generates an 8-channel audio tone file.
2. Produces a video with the test pattern and overlays a timecode.
3. Saves the video in two formats: H.264 (MP4) and ProRes (MOV).

### Prerequisites:

1. Python 3.x installed.
2. `ffmpeg` installed on your system. 

## Usage:

To run the script, navigate to the script directory and use the command:

```bash
python script_name.py [output_base_name] [resolution] [framerate]
```

### Arguments:

* output_base_name: (Optional) The base name for the output files. Default value is "sample".
* resolution: (Optional) Desired video resolution. Default value is "1280x720".
* framerate: (Optional) Desired video frame rate. Default value is "30".


### For instance:

python script_name.py test_video 1920x1080 60

The above command will generate a video named test_video_prores.mov with a resolution of 1920x1080 and a frame rate of 60fps.

### Note:

By default, the script generates only a ProRes formatted video. To generate the video in H.264 format, uncomment the respective line in the script.

The h264 is broken, it has 7 channels instead of 8 and they are not in the good order.
 
