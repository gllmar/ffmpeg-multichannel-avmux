import tkinter as tk
from tkinter import filedialog, messagebox, Toplevel
import sys
import subprocess
import os


def check_if_prores(file_path):
    '''Check if the video is in ProRes format'''
    command = ['ffmpeg', '-i', file_path, '-loglevel', 'error']
    result = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return "ProRes" in result.stderr.decode()

def get_sample_rate(audio_path):
    '''Get sample rate of the audio file'''
    command = ['ffprobe', '-v', 'error', '-select_streams', 'a:0', '-show_entries', 'stream=sample_rate', '-of', 'default=noprint_wrappers=1:nokey=1', audio_path, '-loglevel', 'error']
    result = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    # Check if the stdout result is empty
    if not result.stdout.strip():
        return None

    return int(result.stdout)

def replace_audio(input_video, input_audio, output):
    '''Replace audio in video file using ffmpeg'''

    # Get the sample rate or default to converting to 48kHz if it can't be determined
    sample_rate = get_sample_rate(input_audio)
    if sample_rate is None or sample_rate != 48000:
        if sample_rate is not None:
            print(f"Input audio has a sample rate of {sample_rate}Hz. Conforming to 48kHz.")
        else:
            print("Could not determine sample rate. Conforming to 48kHz.")
        temp_audio = "temp_48khz_audio.wav"
        command = ['ffmpeg', '-i', input_audio, '-ar', '48000', '-acodec', 'pcm_s16le', temp_audio, '-loglevel', 'error', '-y']
        subprocess.run(command)
        input_audio = temp_audio
    else:
        temp_audio = None

    # Check if the input video is already ProRes
    if check_if_prores(input_video):
        # Copy video stream and replace audio without re-encoding video
        command = ['ffmpeg', '-i', input_video, '-i', input_audio, '-c:v', 'copy', '-c:a', 'pcm_s16le', output, '-loglevel', 'error', '-y']
    else:
        # Convert video to ProRes and replace audio with uncompressed PCM audio
        command = ['ffmpeg', '-i', input_video, '-i', input_audio, '-c:v', 'prores', '-c:a', 'pcm_s16le', output, '-loglevel', 'error', '-y']

    subprocess.run(command)

    # Remove temporary audio if it was created
    if temp_audio:
        subprocess.run(['rm', temp_audio])




def choose_file(title):
    '''Prompt user to choose a file and return its path.'''
    root = tk.Tk()
    root.withdraw()
    file_path = filedialog.askopenfilename(title=title)
    return file_path

def show_info():
    '''Open a new window to show the program information.'''
    info_win = Toplevel(root)
    info_win.title("Program Information")

    # Create a label to display the information
    info_label = tk.Label(info_win, text="""Ce programme permet aux utilisateurs de remplacer l'audio d'un fichier vidéo.
Sélectionnez les fichiers vidéo et audio d'entrée. Vous pouvez éventuellement spécifier un nom de fichier de sortie.
Si aucun nom de sortie n'est fourni, le fichier vidéo original sera écrasé.
L'audio sera converti en 48 kHz si son taux d'échantillonnage est différent.
Si la vidéo n'est pas au format ProRes, elle sera convertie en ProRes.
Assurez-vous d'avoir sauvegardé vos fichiers avant de continuer.""", padx=10, pady=10, wraplength=500)
    info_label.pack()

    # Button to close the info window
    close_btn = tk.Button(info_win, text="Close", command=info_win.destroy)
    close_btn.pack(pady=10)

def execute():
    '''Run the main logic when the "Replace Audio" button is clicked.'''
    input_video = video_entry.get()
    input_audio = audio_entry.get()
    output = output_entry.get()

    if not input_video or not input_audio:
        messagebox.showwarning("Warning", "Please select both input video and audio files!")
        return

    if not output:
        output = "temp_output.mov"
    
    # Check if the output file exists and prompt for overwrite
    if os.path.exists(output):
        overwrite = messagebox.askyesno("Confirm Overwrite", f"File {output} already exists. Overwrite?")
        if not overwrite:
            return

    try:
        replace_audio(input_video, input_audio, output)
        if not output_entry.get():
            os.replace(output, input_video)  # Overwrite the original video
        messagebox.showinfo("Success", "Audio replacement completed successfully!")
    except Exception as e:
        messagebox.showerror("Error", f"An error occurred: {str(e)}")

# Set up main GUI window
root = tk.Tk()
root.title("Audio Replacement Tool")

# Create GUI elements
video_label = tk.Label(root, text="Input Video:")
video_entry = tk.Entry(root, width=50)
video_browse_btn = tk.Button(root, text="Browse", 
                             command=lambda: [video_entry.delete(0, tk.END), 
                                              video_entry.insert(0, choose_file("Select Input Video"))])

audio_label = tk.Label(root, text="Input Audio:")
audio_entry = tk.Entry(root, width=50)
audio_browse_btn = tk.Button(root, text="Browse", 
                             command=lambda: [audio_entry.delete(0, tk.END), 
                                              audio_entry.insert(0, choose_file("Select Input Audio"))])

output_label = tk.Label(root, text="Output Video (optional):")
output_entry = tk.Entry(root, width=50)
output_browse_btn = tk.Button(root, text="Browse", 
                              command=lambda: [output_entry.delete(0, tk.END), 
                                               output_entry.insert(0, choose_file("Select Output Video"))])

execute_btn = tk.Button(root, text="Replace Audio", command=execute)

# Create the information button
info_btn = tk.Button(root, text="?", command=show_info, width=2)
info_btn.grid(row=3, column=2, padx=10, pady=10)

# Layout GUI elements
video_label.grid(row=0, column=0, padx=10, pady=10, sticky='w')
video_entry.grid(row=0, column=1, padx=10, pady=10)
video_browse_btn.grid(row=0, column=2, padx=10, pady=10)

audio_label.grid(row=1, column=0, padx=10, pady=10, sticky='w')
audio_entry.grid(row=1, column=1, padx=10, pady=10)
audio_browse_btn.grid(row=1, column=2, padx=10, pady=10)

output_label.grid(row=2, column=0, padx=10, pady=10, sticky='w')
output_entry.grid(row=2, column=1, padx=10, pady=10)
output_browse_btn.grid(row=2, column=2, padx=10, pady=10)

execute_btn.grid(row=3, column=0, columnspan=3, pady=20)

root.mainloop()
