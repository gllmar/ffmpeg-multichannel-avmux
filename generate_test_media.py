import sys
import subprocess
import os

def midi_to_frequency(midi_note):
    """Convert a MIDI note to its frequency in Hz."""
    return 440.0 * 2.0 ** ((midi_note - 69) / 12.0)

def generate_8_channel_audio(audio_file_name):
    # Define MIDI notes for the 8 channels
    midi_notes = [60, 61, 62, 63, 64, 65, 66, 67]
    sample_rate = "48000"  # Setting sample rate to 48kHz

    cmd_audio = ['ffmpeg']

    for midi_note in midi_notes:
        frequency = midi_to_frequency(midi_note)
        cmd_audio.extend([
            '-f', 'lavfi',
            '-i', f"aevalsrc=sin(2*PI*{frequency}*t):d=10:s={sample_rate}"  # Specifying 48kHz
        ])

    cmd_audio.extend([
        '-filter_complex', "amerge=8",
        '-y',
        audio_file_name
    ])
    
    subprocess.run(cmd_audio)
    
def generate_16_channel_audio(audio_file_name):
    # Define MIDI notes for the 16 channels
    midi_notes = [60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 71, 72, 73, 74]
    sample_rate = "48000"  # Setting sample rate to 48kHz

    cmd_audio = ['ffmpeg']

    for midi_note in midi_notes:
        frequency = midi_to_frequency(midi_note)
        cmd_audio.extend([
            '-f', 'lavfi',
            '-i', f"aevalsrc=sin(2*PI*{frequency}*t):d=10:s={sample_rate}"  # Specifying 48kHz
        ])

    cmd_audio.extend([
        '-filter_complex', "amerge=16",
        '-y',
        audio_file_name
    ])
    
    subprocess.run(cmd_audio)


def generate_sample_video(output_base_name, resolution="1280x720", framerate="30"):
    audio_file_name_8 = f'{output_base_name}_8channel_tones.wav'
    if not os.path.exists(audio_file_name_8):
        generate_8_channel_audio(audio_file_name_8)

    audio_file_name_16 = f'{output_base_name}_16channel_tones.wav'
    if not os.path.exists(audio_file_name_16):
        generate_16_channel_audio(audio_file_name_16)

    timecode_filter = (
        f"drawtext=text='%{{pts\\:hms}}.%{{eif\\:t*1000-floor(t*1000+0.5)\\:d=3}}':"
        f"x=(w-tw-10):y=h-(h/3.5)"
    )

    cmd_prores_4444 = [
        'ffmpeg',
        '-f', 'lavfi',
        '-i', f'testsrc=duration=10:size={resolution}:rate={framerate}',
        '-i', audio_file_name_8,
        '-vf', timecode_filter,
        '-c:v', 'prores_ks',
        '-profile:v', '4',  # ProRes 4444
        '-c:a', 'pcm_s16le',
        '-ar', '48000',  # Specifying 48kHz
        f'{output_base_name}_prores_4444.mov'
    ]

    cmd_prores_422 = [
        'ffmpeg',
        '-f', 'lavfi',
        '-i', f'testsrc=duration=10:size={resolution}:rate={framerate}',
        '-i', audio_file_name_8,
        '-vf', timecode_filter,
        '-c:v', 'prores_ks',
        '-profile:v', '2',  # ProRes 422
        '-c:a', 'pcm_s16le',
        '-ar', '48000',  # Specifying 48kHz
        f'{output_base_name}_prores_422.mov'
    ]

    cmd_mp4_8channel = [
        'ffmpeg',
        '-f', 'lavfi',
        '-i', f'testsrc=duration=10:size={resolution}:rate={framerate}',
        '-i', audio_file_name_8,
        '-vf', timecode_filter,
        '-c:v', 'libx264',
        '-profile:v', 'high',  # Suitable profile for QuickTime
        '-level', '4.2',  # Suitable level for QuickTime
        '-pix_fmt', 'yuv420p',  # Pixel format suitable for QuickTime
        '-c:a', 'aac',
        '-ar', '48000',  # Specifying 48kHz
        '-ac', '8',  # 8 channels
        '-map', '0:v',
        f'{output_base_name}_8channel.mp4'
    ]

    cmd_mp4_16channel = [
        'ffmpeg',
        '-f', 'lavfi',
        '-i', f'testsrc=duration=10:size={resolution}:rate={framerate}',
        '-i', audio_file_name_16,
        '-vf', timecode_filter,
        '-c:v', 'libx264',
        '-profile:v', 'high',  # Suitable profile for QuickTime
        '-level', '4.2',  # Suitable level for QuickTime
        '-pix_fmt', 'yuv420p',  # Pixel format suitable for QuickTime
        '-c:a', 'aac',
        '-ar', '48000',  # Specifying 48kHz
        '-ac', '16',  # 16 channels
        f'{output_base_name}_16channel.mp4'
    ]

    subprocess.run(cmd_prores_4444)
    subprocess.run(cmd_prores_422)
    subprocess.run(cmd_mp4_16channel)
    subprocess.run(cmd_mp4_8channel)

def main():
    output_base_name = "sample"
    resolution = "1280x720"
    framerate = "30"
    
    if len(sys.argv) > 1:
        output_base_name = sys.argv[1]
    if len(sys.argv) > 2:
        resolution = sys.argv[2]
    if len(sys.argv) > 3:
        framerate = sys.argv[3]

    generate_sample_video(output_base_name, resolution, framerate)

if __name__ == "__main__":
    main()
