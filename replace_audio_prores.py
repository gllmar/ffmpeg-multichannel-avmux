import subprocess
import sys
import os

def check_if_prores(file_path):
    '''Check if the video is in ProRes format'''
    command = ['ffmpeg', '-i', file_path, '-loglevel', 'error']
    result = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return "ProRes" in result.stderr.decode()

def get_sample_rate(audio_path):
    '''Get sample rate of the audio file'''
    command = ['ffprobe', '-v', 'error', '-select_streams', 'a:0', '-show_entries', 'stream=sample_rate', '-of', 'default=noprint_wrappers=1:nokey=1', audio_path, '-loglevel', 'error']
    result = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return int(result.stdout)

def replace_audio(input_video, input_audio, output):
    '''Replace audio in video file using ffmpeg'''

    # Check if the input audio has 48kHz sample rate. If not, convert it to 48kHz.
    sample_rate = get_sample_rate(input_audio)
    if sample_rate != 48000:
        print(f"Input audio has a sample rate of {sample_rate}Hz. Conforming to 48kHz.")
        temp_audio = "temp_48khz_audio.wav"
        command = ['ffmpeg', '-i', input_audio, '-ar', '48000', '-acodec', 'pcm_s16le', temp_audio, '-loglevel', 'error']
        subprocess.run(command)
        input_audio = temp_audio
    else:
        temp_audio = None

    # Check if the input video is already ProRes
    if check_if_prores(input_video):
        # Copy video stream and replace audio without re-encoding video
        command = ['ffmpeg', '-i', input_video, '-i', input_audio, '-c:v', 'copy', '-c:a', 'pcm_s16le', output, '-loglevel', 'error']
    else:
        # Convert video to ProRes and replace audio with uncompressed PCM audio
        command = ['ffmpeg', '-i', input_video, '-i', input_audio, '-c:v', 'prores', '-c:a', 'pcm_s16le', output, '-loglevel', 'error']

    subprocess.run(command)

    # Remove temporary audio if it was created
    if temp_audio:
        subprocess.run(['rm', temp_audio])

if __name__ == '__main__':
    if len(sys.argv) == 3:
        input_video = sys.argv[1]
        input_audio = sys.argv[2]
        temp_output = "temp_output.mov"
        replace_audio(input_video, input_audio, temp_output)
        os.replace(temp_output, input_video)  # Overwrite the original video
    elif len(sys.argv) == 4:
        input_video = sys.argv[1]
        input_audio = sys.argv[2]
        output = sys.argv[3]
        replace_audio(input_video, input_audio, output)
    else:
        print("Usage: python script_name.py input_video input_audio [output]")
        sys.exit(1)
