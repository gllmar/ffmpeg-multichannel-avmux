import subprocess
import os
import re
import tkinter as tk
from tkinter import filedialog, messagebox
from tkinterdnd2 import TkinterDnD, DND_FILES
import threading

# Custom colors and styles
BG_COLOR = "#2E3440"
FG_COLOR = "#ECEFF4"
BTN_COLOR = "#5E81AC"
BTN_HOVER_COLOR = "#81A1C1"
PROGRESS_COLOR = "#88C0D0"
LABEL_COLOR = "#D08770"
FONT = ("Arial", 12)
TITLE_FONT = ("Arial", 16, "bold")


def check_if_prores(file_path):
    '''Check if the video is in ProRes format'''
    command = ['ffmpeg', '-i', file_path, '-loglevel', 'error']
    result = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return "ProRes" in result.stderr.decode()


def get_sample_rate(audio_path):
    '''Get sample rate of the audio file'''
    command = ['ffprobe', '-v', 'error', '-select_streams', 'a:0', '-show_entries', 'stream=sample_rate', '-of', 'default=noprint_wrappers=1:nokey=1', audio_path, '-loglevel', 'error']
    result = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return int(result.stdout)


def get_video_duration(file_path):
    '''Get video duration in seconds using ffprobe'''
    command = ['ffprobe', '-v', 'error', '-show_entries', 'format=duration', '-of', 'default=noprint_wrappers=1:nokey=1', file_path]
    result = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return float(result.stdout)


def replace_audio(input_video, input_audio, output, progress_callback):
    '''Replace audio in video file using ffmpeg and update progress'''

    sample_rate = get_sample_rate(input_audio)
    temp_audio = None

    # Convert audio to 48kHz if necessary
    if sample_rate != 48000:
        print(f"Input audio has a sample rate of {sample_rate}Hz. Conforming to 48kHz.")
        temp_audio = "temp_48khz_audio.wav"
        command = ['ffmpeg', '-i', input_audio, '-ar', '48000', '-acodec', 'pcm_s16le', temp_audio, '-loglevel', 'error']
        subprocess.run(command)
        input_audio = temp_audio

    # Determine FFmpeg command based on ProRes check
    if check_if_prores(input_video):
        command = ['ffmpeg', '-i', input_video, '-i', input_audio, '-c:v', 'copy', '-c:a', 'pcm_s16le', output, '-progress', '-', '-nostats', '-loglevel', 'error']
    else:
        command = ['ffmpeg', '-i', input_video, '-i', input_audio, '-c:v', 'prores', '-c:a', 'pcm_s16le', output, '-progress', '-', '-nostats', '-loglevel', 'error']

    # Run FFmpeg with progress monitoring
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)

    # Monitor progress
    for line in process.stdout:
        if "out_time_ms" in line:
            time_elapsed = int(re.search(r'out_time_ms=(\d+)', line).group(1)) / 1000000.0  # Convert microseconds to seconds
            progress_callback(time_elapsed)

    process.wait()

    # Clean up temp file
    if temp_audio:
        os.remove(temp_audio)


class RoundedButton(tk.Canvas):
    """ Custom rounded button """
    def __init__(self, parent, text, command, width=150, height=50, corner_radius=25, *args, **kwargs):
        tk.Canvas.__init__(self, parent, width=width, height=height, bg=BG_COLOR, bd=0, highlightthickness=0, relief='flat')
        self.command = command
        self.corner_radius = corner_radius
        self.width = width
        self.height = height
        self.text = text
        self.draw_button()
        self.bind("<ButtonPress-1>", self.on_click)
        self.bind("<Enter>", self.on_hover)
        self.bind("<Leave>", self.on_leave)

    def draw_button(self):
        self.delete("all")
        self.create_oval((0, 0, 2 * self.corner_radius, 2 * self.corner_radius), fill=BTN_COLOR, outline=BTN_COLOR)
        self.create_oval((self.width - 2 * self.corner_radius, 0, self.width, 2 * self.corner_radius), fill=BTN_COLOR, outline=BTN_COLOR)
        self.create_oval((0, self.height - 2 * self.corner_radius, 2 * self.corner_radius, self.height), fill=BTN_COLOR, outline=BTN_COLOR)
        self.create_oval((self.width - 2 * self.corner_radius, self.height - 2 * self.corner_radius, self.width, self.height), fill=BTN_COLOR, outline=BTN_COLOR)
        self.create_rectangle((self.corner_radius, 0, self.width - self.corner_radius, self.height), fill=BTN_COLOR, outline=BTN_COLOR)
        self.create_rectangle((0, self.corner_radius, self.width, self.height - self.corner_radius), fill=BTN_COLOR, outline=BTN_COLOR)
        self.text_id = self.create_text(self.width / 2, self.height / 2, text=self.text, font=FONT, fill=FG_COLOR)

    def on_click(self, event):
        self.command()

    def on_hover(self, event):
        self.itemconfig(self.text_id, fill=BTN_HOVER_COLOR)

    def on_leave(self, event):
        self.itemconfig(self.text_id, fill=FG_COLOR)


class AudioVideoApp(TkinterDnD.Tk):
    def __init__(self):
        super().__init__()
        self.title("Modern Video Audio Replacer")
        self.geometry("600x600")
        self.configure(bg=BG_COLOR)

        self.grid_columnconfigure(0, weight=1)  # Make column expandable
        self.grid_rowconfigure(6, weight=1)  # Make bottom row expandable

        self.video_file = ""
        self.audio_file = ""
        self.video_duration = 0

        self.create_widgets()

    def create_widgets(self):
        # Title Label
        title_label = tk.Label(self, text="Video Audio Replacer", bg=BG_COLOR, fg=FG_COLOR, font=TITLE_FONT)
        title_label.grid(row=0, column=0, padx=10, pady=20, sticky="n")

        # Video file drag-and-drop (2 times taller)
        self.video_label = tk.Label(self, text="Drag Video Here", relief=tk.RIDGE, height=6, bg=LABEL_COLOR, fg=FG_COLOR)
        self.video_label.grid(row=1, column=0, padx=10, pady=10, sticky="ew")
        self.video_label.drop_target_register(DND_FILES)
        self.video_label.dnd_bind('<<Drop>>', self.handle_video_drop)

        # Display video file path
        self.video_path_label = tk.Label(self, text="", bg=BG_COLOR, fg=FG_COLOR, font=FONT)
        self.video_path_label.grid(row=2, column=0, padx=10, pady=5, sticky="ew")

        # Audio file drag-and-drop (2 times taller)
        self.audio_label = tk.Label(self, text="Drag Audio Here", relief=tk.RIDGE, height=6, bg=LABEL_COLOR, fg=FG_COLOR)
        self.audio_label.grid(row=3, column=0, padx=10, pady=10, sticky="ew")
        self.audio_label.drop_target_register(DND_FILES)
        self.audio_label.dnd_bind('<<Drop>>', self.handle_audio_drop)

        # Display audio file path
        self.audio_path_label = tk.Label(self, text="", bg=BG_COLOR, fg=FG_COLOR, font=FONT)
        self.audio_path_label.grid(row=4, column=0, padx=10, pady=5, sticky="ew")

        # Progress bar
        self.progress_var = tk.DoubleVar()
        self.progress_bar = tk.ttk.Progressbar(self, variable=self.progress_var, maximum=100, mode='determinate')
        self.progress_bar.grid(row=5, column=0, padx=10, pady=20, sticky="ew")

        # Process button (Custom Rounded Button)
        self.process_button = RoundedButton(self, text="Replace Audio", command=self.process_files)
        self.process_button.grid(row=6, column=0, padx=10, pady=20, sticky="ew")

    def handle_video_drop(self, event):
        self.video_file = event.data.strip("{}")
        if os.path.isfile(self.video_file):
            self.video_duration = get_video_duration(self.video_file)
            self.video_label.config(text=f"Video: {os.path.basename(self.video_file)}")
            self.video_path_label.config(text=f"Path: {self.video_file}")  # Display full video path
        else:
            messagebox.showerror("Error", "Invalid video file.")

    def handle_audio_drop(self, event):
        self.audio_file = event.data.strip("{}")
        if os.path.isfile(self.audio_file):
            self.audio_label.config(text=f"Audio: {os.path.basename(self.audio_file)}")
            self.audio_path_label.config(text=f"Path: {self.audio_file}")  # Display full audio path
        else:
            messagebox.showerror("Error", "Invalid audio file.")

    def update_progress(self, time_elapsed):
        '''Update progress bar based on time elapsed'''
        if self.video_duration > 0:
            progress_percentage = (time_elapsed / self.video_duration) * 100
            self.progress_var.set(progress_percentage)
            self.update_idletasks()

    def process_files(self):
        if not self.video_file or not self.audio_file:
            messagebox.showwarning("Missing Files", "Please provide both video and audio files.")
            return

        # Choose output file location
        output_file = filedialog.asksaveasfilename(defaultextension=".mov", filetypes=[("MOV files", "*.mov")], title="Save output as")
        if not output_file:
            return

        # Run the process in a separate thread to avoid blocking the UI
        threading.Thread(target=self.run_ffmpeg_process, args=(output_file,)).start()

    def run_ffmpeg_process(self, output_file):
        try:
            replace_audio(self.video_file, self.audio_file, output_file, self.update_progress)
            messagebox.showinfo("Success", f"Audio replaced successfully.\nSaved at: {output_file}")
        except Exception as e:
            messagebox.showerror("Error", f"An error occurred during processing: {str(e)}")
        finally:
            # Reset progress bar after completion
            self.progress_var.set(0)


if __name__ == "__main__":
    import tkinter.ttk as ttk
    app = AudioVideoApp()

    # Style for progress bar
    style = ttk.Style(app)
    style.theme_use('clam')
    style.configure("TProgressbar", troughcolor=BG_COLOR, background=PROGRESS_COLOR, thickness=20)

    app.mainloop()
